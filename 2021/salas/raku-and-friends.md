---
layout: 2021/post
section: proposals
category: devrooms
community: Raku & Perl Mongers
title: Raku & Friends
---

## Devroom description

[Raku](https://raku.org) is a multi-paradigm language _created for the next 100 years_. Its wealth of features and expressivity make it an ideal choice for creative programming. We consider friends most other languages, or all of them, really, but if you need to ask, we would be interested in technologies or services that could be interfaced with Raku (data stores, APIs), or programming techniques (grammars, regular expressions, functional programming, concurrent programming) that could be of interest for the Raku community, even if code samples are not written in the language. In a word, we want to attract any programmer that enjoys their job, and wants to inspire and be inspired by other people like them.

In this edition, this devroom is going to be composed of several short keynotes, that were invited to propose a talk.

## Community that proposes it

Raku and Perl are sister languages created by Larry Wall. They have in common their whippipitude and the TIMTOWDI motto: there are many ways to do it, just find your own. Raku is a younger language, out of beta since 2015, but since then it has grown to be used by a growing community of enthusiasts, and also companies.

#### Raku & Perl mongers

-  Community website: <https://raku.org>
-  Mastodon (or other free social networks):
-  Twitter:
-  Gitlab:
-  Portfolio or GitHub (or other collaborative code sites): <https://github.com/Raku>

### Contact/s

-  Contact name: JJ Merelo
-  Contact email: <jjmerelo@gmail.com>

## Target audiences

English-speaking coding enthusiast, with or without any prior experience with the language.

## Format

In this edition, this devroom is going to be composed of several short keynotes, that were invited to propose a talk.

The devroom will start with a short intro by JJ Merelo.

<h3 id="vue+raku">Cecilia Merelo: <em>Vue + Raku, a new facade</em> (40 minutes)</h3>

Raku is usually used as backend language so let's give a frontend to our Raku scripts!

**Cecilia Merelo** (she/her) is junior software engineer in Badger Maps.

<h3 id="designing-in-meeting-indicator">Joelle Maslak: <em>Designing a 'In-Meeting' indicator with Raku</em> (20 minutes)</h3>

Joelle talks about the design decisions behind her "busy indicator" light, which lets others know if she is in a meeting or if she can be interrupted. Her busy indicator script will be discussed, showing how Raku is an exceptional glue language, and can tie together calls to external executables, operating system status information, network access, and USB device interaction easily. I expect this talk to be interesting for all skill levels, including beginners.

**Joelle Maslak** (she/her) is Network Engineer @ Netflix (Open Connect)

<h3 id="malware-analysis-scripting">Paula de la Hoz: <em>Malware analysis scripting with Raku</em> (20 minutes)</h3>

When it comes to study malware and cyber threats, some cases aren't as clear as others an require further analysis of the samples. For this purpose, some interesting and well known tools such as Yara, Cutter, Intezer and others are widely used, but for the sake of self-made I will introduce a brief Raku scripting for malware analysis using rules. This will help iterate a binary using the mere Terminal looking for malicious cyber-genes.

**Paula de la Hoz** (she/her) is Threat Hunting analyst in Telefonica Tech, [Interferencias](https://interferencias.tech) co-founder.

<h3 id="sigils-raku">Liz Mattijsen: <em>Sigils - Once you know them, you'll love them in the Raku Programming Language</em> (20 minutes)</h3>

The roots of the Raku Programming Language are in Perl. The use of sigils in Perl is seen by many as the reason it is hard to read any code written in Perl. In this presentation, I will show how sigils can be used in the Raku Programming Language, and how they actually *help* in making code understandable. And should you decide you don't like sigils after all, how you can make Raku programs without them.

**Elizabeth Mattijsen** (she/her) is Raku Programming Language core developer, member of the Raku Steering Council and Former recipient of the White Camel Award.

<h3 id="pattern-nativecall-bindings">Clifton Wood: <em>A pattern for creating NativeCall bindings</em> (30 minutes)</h3>

Or, how he wrote around 560000 lines of Raku in 3 years.

**Clifton Wood** is a frequent Raku contributor.

## Comments
