---
layout: 2021/post
section: proposals
category: talks
author: Juan Febles
title: ffmpeg el comando único multimedia para controlarlo todo
---

A través de varios argumentos y sentencias, vamos a sacarle todo el partido a ffmpeg, la navaja suiza multimedia que todos tenemos a golpe de teclas: audio, vídeo...

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Vamos a realizar varias sentencias del comando como ejemplos:

Extraer audio de vídeo, cambiar audio de vídeo, cortar vídeos o audios, concatenar varios vídeos, cambiar a varios formatos, realizar audiogramas, etc...

-   Web del proyecto:

## Público objetivo

Todos los que quieran quitarse el miedo a la terminal y vean la capacidad que tiene.

## Ponente(s)

Juan Febles es maestro de Educación Especial y amante del Software Libre. Produce Podcast Linux, un programa de divulgación del Software Libre y GNU/Linux.

### Contacto(s)

-   Nombre: Juan Febles
-   Email: <podcastlinux@disroot.org>
-   Web personal: <https://podcastlinux.com/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@podcastlinux/>
-   Twitter: <https://twitter.com/podcastlinux>
-   GitLab: <https://gitlab.com/podcastlinux>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios
