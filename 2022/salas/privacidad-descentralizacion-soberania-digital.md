---
layout: 2022/post
section: proposals
category: devrooms
author: Trackula
title: Privacidad, descentralización y soberanía digital
---

# Privacidad, descentralización y soberanía digital

## Detalles de la propuesta:

-   Descripción:

> En un mundo con cada vez más tecnología, cada día tenemos menos poder sobre lo que ocurre con nuestra información personal.<br><br>
El poder que el Software Libre nos dio para con nuestros dispositivos, la economía de la vigilancia nos lo está arrebatando con la centralización de servicios en manos de empresas cuyo modelo de negocio es venderte.<br><br>
En consecuencia, nuestras soberanía democrática, social e individual se están viendo mermadas y coartadas al permitir (a veces sin apenas pensarlo) que se traten nuestros datos, muchas veces de forma  desproporcionada, y con consecuencias que todavía  no podemos ni anticipar.
* ¿Qué podemos hacer al respecto?
* ¿Cómo podemos imaginar un futuro de la tecnología diferente, más inclusivo y menos abusivo?
* ¿De qué herramientas y servicios alternativos disponemos?
* ¿Cómo educamos a la sociedad para que quiera defenderse?<br><br>
Buscamos para la devroom contribuciones que indaguen en los problemas  que surgen respecto a estos temas, y también sobre proyectos y  alternativas que respeten los derechos fundamentales de las personas y  que nos hagan más soberanos o conscientes con la tecnología, de forma general.<br><br>
Nos interesan mucho los siguientes temas, pero no dudes en proponer otros que puedan encajar aunque no estén especificados en la lista. ¡Estamos totalmente abiertos a propuestas!<br><br>
Tecnologías, proyectos e iniciativas basadas en la privacidad:<br><br>
* Como crear conciencia ciudadana al respecto
* Redes anónimas, Friend-to-friend, o Dark Nets
* Sistemas operativos pro-privacidad
* Infraestructura y tecnología para la anonimidad en la red
* Seguridad operacional: como reducir la cantidad de info. que se recopila de ti?
* Modelos de amenaza: hasta dónde debería de llegar tu paranoia?
* Pasos para minimizar el impacto en una brecha de privacidad<br><br>
Tecnologías, proyectos e iniciativas basadas en la descentralización:<br><br>
* Como crear conciencia ciudadana al respecto
* Redes descentralizadas y comunitarias
* Mecanismos que no requieran confianza para realizar operaciones seguras
* Comunidades de self-hosting
* Soluciones para el self-hosting de tus propios servicios
* Redes, algoritmos y protocolos de red descentralizados
* Aplicaciones descentralizadas para servicios finales, como  mensajería instantánea, almacenamiento de backups, compartición de  documentos o páginas web, colaboración en tiempo real...<br><br>
Tecnologías, proyectos e iniciativas basadas en la soberanía digital<br><br>
* Como crear conciencia ciudadana al respecto
* Redes pro-soberanía digital
* Sistemas de votación electrónica con garantías democráticas
* Tecnología para permitir a la ciudadanía el control en los procesos democráticos
* Proyectos para incrementar la Transparencia en organizaciones
* Sobre control individual o social de los datos personales
* Sobre la elaboración de perfiles ciudadanos y su impacto

-   Formato:

>
* Charlas de 40 minutos
* OpenSpace
* Mesa redonda grabada que será un capítulo de Nada que Esconder<br><br>
Sábado 25 de 10h a 14h y de 15.30 a 19.30h

-   Público objetivo:

> Nuestro público objetivo es muy variado, desde gente que se aproxima a la privacidad por su formación técnica hasta personas que están preocupadas por su relación directa y diaria con la tecnología sin tener una formación específica en informática.

## Comunidad que propone la sala:

-   Nombre: Trackula
-   Info:

> Somos un grupo informal de personas preocupadas por la privacidad, la tecnología libre y la soberanía tecnológica.<br><br>
Todo surgió cuando nos juntamos en 2017 en una convocatoria del programa Visualizar en MediaLab Prado donde desarrollamos un plugin para Firefox <https://trackula.org/es/page/landing-plugin/> con la idea de mostrar cómo las cookies nos persiguen por la web.<br><br>
Durante estos años nos hemos centrado mucho en la concienciación, organizando charlas en #DistopíasCotiás (A Coruña), una devroom en esLibre y seguimos tratando de participar en todo tipo de eventos como FOSDEM pero somos muy de pasar a la acción y queremos también dar soluciones que mejoren el mundo.<br><br>
También hemos organizado La Escondite <https://laescondite.com/> que pretendía ser un evento anual que ayudara a empezar una comunidad de gente preocupada por estos temas, pero la pandemia nos obligó a cambiar los planes y pasarnos a un formato diferente y online que no acabó de cuajar demasiado.<br><br>
Ahora mismo tenemos varios proyectos activos como es Techtopias <https://www.techtopias.com/>, una newsletter quincenal que agrega y resume las noticias en el campo de la privacidad y la soberanía tecnológica y también Nada Que Esconder <https://blog.iuvia.io/tag/podcast/>, un podcast mensual sobre las mismas temáticas y donde tratamos de dar voz a personas referentes de nuestro mundillo.<br><br>
También hemos tratado de arrancar IUVIA <https://iuvia.io/>, una alternativa libre y descentralizada a la nube tradicional, pero los problemas de suministro de chips nos han complicado las cosas y hemos tenido que parar el proyecto.<br><br>
<img src="/assets/logos/2022/trackula.png" alt="Logo Trackula" width="700">
<img style="padding-left: 3em" src="/assets/logos/2022/gpul.png" alt="Logo GPUL" height="300">

-   Web: <https://trackula.org/es/>
-   Twitter: <https://twitter.com/trackula_>
-   GitLab (o cualquier sitio de código colaborativo) o portfolio: <https://gitlab.com/trackula>

## Condiciones aceptadas

-   [x]  Aceptamos seguir el código de conducta (<https://eslib.re/conducta>) durante nuestra participación en el congreso
