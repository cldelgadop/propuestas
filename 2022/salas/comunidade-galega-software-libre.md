---
layout: 2022/post
section: proposals
category: devrooms
author: A. Melisa
title: A comunidade galega de software libre
---

# A comunidade galega de software libre

## Detalles de la propuesta:

-   Descripción:

> Mesa redonda para debatir o pasado e presente das diferentes actividades levadas a cabo polas asociacións de usuarios de software libre en Galicia, e cara onde imos no futuro próximo. Nun primeiro momento a mesa estará formada por xente relacionada co asociacionismo.
O/a moderador/a desta mesa terá un perfil de comunicador/xornalista.

-   Formato:

> Mesa redonda

-   Público objetivo:

> Persoas interesadas en asistir (ou participar) a un debate aberto onde se expoñan accións a prol da difusión do software libre en Galicia (resto de comunidades).

## Comunidad que propone la sala:

-   Nombre: A. Melisa

-   Info:

> Asociación de Usuarios de Software Libre da Terra de Melide.

-   Web: <https://www.melisa.gal>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
