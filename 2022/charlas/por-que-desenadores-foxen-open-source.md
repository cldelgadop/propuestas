---
layout: 2022/post
section: proposals
category: talks
author: Sabela Muñiz
title: Por que os deseñadores foxen dos programas Open Source?
---

# Por que os deseñadores foxen dos programas Open Source?

> Análise e reflexión do por qué a maoría de deseñadores prefiren traballar con programas de deseño privativos.

## Detalles de la propuesta:

-   Tipo de propuesta: Charla corta / Presencial

-   Descripción:

> Farei unha breve exposición sobre as causas que levan ós deseñadores a decantarse polos programas de deseño privativos. Os pros e contras cos que se atopan os que deciden facer a transición ós programas Open Source máis empregados. E unha posible solución para facer máis apetecible o seu emprego.

-   Público objetivo:

> En principio está orientada a todos os públicos. Supoño que levantará máis interese nos deseñadores, pero gustaríame atraer tamén o interese dos desenvolvedores.

## Ponente:

-   Nombre: Sabela Muñiz

-   Bio:

> Son tradutora e creativa dixital. Colaboro con proxectos tecnolóxicos que queiran mellorar a vida das persoas, comprometidos co medio ambiente e coa sociedade. Gústame combatir a fenda dixital a través de cursos ou charlas, cunha linguaxe próxima e que poida entender calquera.<br><br>
Experiencia na materia... levo empregando programas de edición de imaxes, edición de gráficos vectoriais, edición de vídeo, deseño e modelado 3D, entre outros, dende fai case 20 anos.<br><br>
Participei como ponente na WordCamp de Pontevedra e na de Galicia. Na Blendiberia e na Women Techmaker, entre outros.

### Info personal:

-   Twitter: <https://twitter.com/castrovilar>

## Condiciones aceptadas

-   [x]  Acepto seguir el código de conducta (<https://eslib.re/conducta>) durante mi participación en el congreso
